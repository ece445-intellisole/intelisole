# 2022/2/2
Today's discussion contained how to approach our senior designa and assign subsystems to each individuals.

## Subsystem 3: Signal Processing(Me and Ritvik)
Resistive Sensing Design Guide:
https://www.tarthorst.net/reading-resistive-sensors-with-microcontrollers/ 
https://learn.sparkfun.com/tutorials/analog-to-digital-conversion/relating-adc-value-to-voltage 

MCU
https://www.ti.com/product/MSP430F5659#design-development

## Subsystem 4: Data Storage(Me)
SD/microSD Interfacing guide with microcontroller
https://openlabpro.com/guide/interfacing-microcontrollers-with-sd-card/

## Subsystem 5: Data Transmission(Me and Ritvik)
Bluetooth Module
https://developer.android.com/guide/topics/connectivity/bluetooth


# 2022/2/14
Today's meeting finalized parts orders and circuitory design.
Component
Subsystem
Total Item Cost (Quantity)
LM324PWRG3 Operational Amplifier
Pressure Data Acquisition
$4.04 (x10)
FSR 402 Force Sensing Resistor
Pressure Data Acquisition
$51.84 (x6)
BAT-HLD-003-SMT Battery Retainer
## Power Distribution
$1.84 (x4)
36-1058-ND Battery Holder
Power Distribution
$4.92 (x4)
CR2032 Battery
Power Distribution
$4.66 (x12)
TPS62203 Voltage Step-Down
## Power Distribution
$5.22 (x3)
ESP32-DEVKITC-32D
Signal Processing
$19.94 (x2)
ESP32-U4WDH
## Signal Processing
$6.21 (x3)
HR1964TR-ND Micro-SD Connector
## Data Storage
$2.50 (x1)
AP-MSD256ISI-1T Micro-SD
Data Storage
$12.92 (x2)

TOTAL: $116.65
Through my own ECE engr portal, I purchased those parts on digikeys and mouser. This order is approved later.
![](Purchase_Order.png)

# 2022/2/14
In today's work, we finished our design documents and RV tables. 
The supply voltage needs to be within the range of 3.3V ±5% and power 1.5mA of current during write cycles
1A. Using an oscilloscope, ensure the power supplied is above 3.3V ±5%.
1B. Using a multimeter, verify the power draw during write cycles is below 1.5mA
The system would be able to detect and eliminate noises in data. 
2A. Simulate a noise signal and add it to one of the lines. Then check on an oscilloscope to see if the signal is filtered out.

256KB of data sent from MCU via Bluetooth is received by software.	
1A. Check to see if Bluetooth data is being transmitted at a rate of 2.4 GHz. 
1B. Analyze data received by software via Bluetooth for any missing/corrupt sensor readings within the packetized 6x10 array.
Successfully pair with an Android device.		
2A. See if IntelliSole is recognized as a Bluetooth device on the software host.

Software must retrieve correct sensor values and not lose any timestamp of data in the process. 
1A. Read through ADC sensor data stored in SD card (or Bluetooth) by . 
1B. Check to see any corruption of values within the 6x10 array.
Given sensor data, the front end must display the recorded data in the form of a heatmap.
2A. Use the android studio debugger to step through the visualization of the pressure heatmap. 
2B. Verify the playback on the user interface by replaying the same data again.

# 2022/3/15

In today's meeting, we modified our subsystem expectations. Data storage subsystem is decided optional for that testing is only available after PCB manufacture. 

# 2022/3/28

Today, we participated in the Pressure Data Acquisition subsystem verification by helping establish the pressure sensor’s linear response zone. Now the Pressure Data Acquisition subsystem can read valid pressure values and the circuit can convert resistance value to voltage values. One of the key problems is to establish a linear response zone for the pressure sensor so that we can decide the validity of pressure data later in the signal processing stage and also build a force/pressure to voltage relation based on this. Due to the limits in the lab, we decided to approach this problem by filling up a water cup with water gradually and building a plot with the voltage values read and amount of water in the cup. As seen in the figure, the cup itself is weighing 4.5 gram, and we fill the cup each time with ¼ cup of water which is roughly 60 milliliter until the cup is full which contains 250 ml of water.
![](weighing_cup.jpeg)
![](cup_weight.jpg)
![](Linear_table.png)   
  
### According to the linear regression model, the model has mean square error of 13.88 and regression p_value of 0.00083 which is considered statistically significant for the model to be considered linear.  

![](linear_regression.png)  
  

# 2022/4/3  

### I verified microcontroller functionality by writing a code to scan WIFI, which returned satisfying result.
![](WIFI_scan.png) 

# 2022/4/4

### I also participated in the Bluetooth Data Transfer subsystem and User Interface subsystem to establish bluetooth connection. 
Bluetooth Data Transfer subsystem can also be enabled and connect to a third party application. My next step will be to continue to work on the bluetooth module and verify data flow acquired by the sensor can be transmitted to our frontend application. I will also need to build a frontend script that handles the input and convert data into a plot. The last thing needed to do is to build a Data Storage subsystem that can store the data when bluetooth function is disabled.  
![](nRF.png) 

# 2022/4/5

Today I worked on finiding a good bluetooth low engery library for Andorid Studio frontend development. I decided to go with this one as it has minimal functionality and it's good for tesing and remodifying.

https://github.com/ederdoski/SimpleBle

# 2022/4/7

The above repository is not suited to continue to work on so I decided to use this source instead for frontend development.

https://github.com/Jasonchenlijian/FastBle

# 2022/4/10

Today I continue to work on bluetooth connection on Micro-controller. I found this tutorial very helpful.

https://randomnerdtutorials.com/esp32-bluetooth-low-energy-ble-arduino-ide/  

I also worked on how to make multiple characteristics and it has some problems not setting up servers correctly. This issue is solved by using online UUID generators website to initialize different channels.  

https://www.uuidgenerator.net/  

# 2022/4/14

I worked on transferring my work space from arduino IDE to platform IO on visual Studio code to speed things up.

https://platformio.org/?utm_source=platformio&utm_medium=piohome

I have an issue of not detecting usb to uart port which leads to me not able to uplad the code to our microcontroller. This is solved by downloading corresponding port helper app and use another mcrousb cord.

# 2022/4/16

Today I modified my bluetooth code and now it reads in voltage values from 6 different pins and set the values in each channels. The current problem right now is that microcontroller reads in 12 bit integer and and BLE protocol accepts string(array) or 8bit integer. We have to scale down based on our results in signal processing. Our current method is right shift 4 bits but the signal we are getting is very unstable.

# 2022/4/17

Today I toucheed on the data storage subsystem and I decided to write four helper functions that recognizes the sd card connetion, writes data into buffer, writes buffer into file and create new file every time it is initialized. The reference I am using is this: https://randomnerdtutorials.com/esp32-data-logging-temperature-to-microsd-card/  

# 2022/4/19

In the mock demo, I am aware that our frontend development is behind schedule so this will be my main focus this week.

# 2022/4/20

Today I finalized the embedded code for our microcontroller and uploaded to gitlab repository.  https://gitlab.engr.illinois.edu/ece445-intellisole/intelisole/-/tree/main/code   

We are habing trouble uploading our code to the pcb circuit so Ritvik decided for me to upload the code to the devkit today and he will desolder the microcontroller on this devkit and resolder it onto our PCB. Because we are having trouble uploading code to pcb, we decided not to proceed with data storage subsytem since  ### 1 We have a functioning data transmission subsystem and it is not necessary for another data storage subsystem. ### 2 Without the functioning pcb, I cannot connect micro sd onto our circuit to test the functionlity.

# 2022/4/21

Today I finalized data transmission subsystem's uintegration with signal processing subsystem. We are storing 16 12-bit voltage values in a 16 bit buffer variable. Calculate the average each time the buffer is full and update the value in the ble servers' corresponding characteristics.

![](sigpro.png)

Today I also started to work on ontegrating data visualization into our frontend application. I don't have enough time for creating a heap map so we decided to have a grpah of pressure v. time. Ritvik provided me with a couple of useful libraries to use.

https://github.com/jjoe64/GraphView

# 2022/4/22

I ran into a problem of not being able to display graphs on the frontend application. I debugged for a long time and didn't see any errors. At the very end, Ritvik found out that I have another linear layout on top of graphview and it blocks the graphview from being visible. Thank you Ritvik!!!

# 2022/4/23

Today is the last day before demo. We are asked by the professor to have a fixed label on the graph and I changed the corresponding parameter to make it happen. Reference: https://www.tabnine.com/code/java/classes/com.jjoe64.graphview.GraphView

# 2022/4/28

Today we met up and finished pur slides for presentation. We are getting positive feedbacks from mock presentation and we are happy with our display.
