# Alkesh Sumant's Worklog

[[_TOC_]]

# 2/6/2022 - Subsystem Delegations and Design for Proposal

We delegated responsibility for the various subsytems in our design, and finalized the material needed for our project proposal:

Alkesh - Sensor systems and Design
Ritvik - Power distribution
William - Data Storage and Software



# 2/15/2022 - Planning and Discussion

The meeting objective was to plan out the projects for this and next week, and look over the requirements for the Design Document.
We had registered our Design Document review to be next Tuesday @ 11am with Prof. Schuh. We chose to meet with him as his background in power electronics will help us gain feedback regarding our power distribution system, as none of us have extensive experience in designing such systems.

We had also planned our work timelines for this week. Our priority is to finalize the circuitry required for our sensor implementation, iron out the subsystems design and begin ordering parts. 


# 2/16/2022 - Sensor Circuitry and Design

Through our investigations today, we have determined that there are two types of sensors most ideal for our design: Force-sensing resistors (FSRs), or developing a pressure-sensing matrix array.

# 2/17/2022 - Deciding Between Sensor Options

Currently, we are deciding between developing a pressure-sensing matrix array as the shoe insole or purchasing FSRs from online. The Pros and Cons are weighted below:

PRESSURE-SENSING MATRIX : PROS
- Maximum coverage of foot
- Sensor construction isn't complex
- High theoretical range of sensitivity, based on online demonstrations

PRESSURE-SENSING MATRIX : CONS
- Make the sensor ourselves
- More room for error (as we're making the sensor)
- Increased design complexity, since each copper line requires its own amplification circuitry
- Testing to establish range of linearity

Based on this info, it will make more sense to implement FSRs into an existing shoe insole. The design/testing complexity seems too great to pursue the pressure-sensing matrix route, and we cannot proceed with other subsystems until we know that the pressure sensors work. 

# 2/18/2022 - Design Doc Signup

Confirmed date with Hanyin Shao regarding design doc check.

# 2/20/2022 - Design Doc Progress

Finished writing all sections related to the Data Acquisition system. Topics finished included writing the subsystem overview on pressure data acquisition, where I discuss the layout for placing FSRs across our shoe insole, the circuitry for amplifying the voltage response of these FSRs, and the requirements and verifications for ensuring that this subsytem performs as desired. The high-level requirement may need some revision, but will wait for Prof. Schuh's feedback before changing anything.

# 2/21/2022 - LTspice Sims for Amplification Circuit

Made changes to the Design Doc after the results of the LTspice sims.

Originally, I was opting for a simple voltage-divider circuit, in which the change in voltage caused by the change in resistance of the FSRs will be fed through a generic op-amp. However, I had found that the response was largely binary. When the FSR was fully pressed, Vout has a maximum of 4V. When not pressed at all, response is at 0 V. There is an exponentially decaying relationship between the FSR resistance and Vout, which will yield innacuracies when doing voltage-pressure conversions. 

Based on this response, I need to find a way in which the voltage gain can be adjusted and we can acheive a linear regime between FSR resistance and Vout.

I instead opted for an adjustable buffer circuit, in which the circuit gain can be adjusted by a ratio of Rin/Rout. Rin refers to the resistance of the op-amp input, and Rout is the resistance of the op-amp output. Rin should not be modified, as the FSR resistance is already a changing component and having a potentiometer connected from the op-amp input to ground will only increase design complexity. As such, we can tune the value of Rout to adjust the output gain more effectively.
This can be done by connecting a resistor between the inverted op-amp input and the output, and one between the inverted op-amp input and ground. I'll refer to these two resistors as R2 and R1. Instead of using a potentiometer, I can adjust the ratio between R2 and R1 such that I meet the following requirements:
- Maximum output gain is 3.3V
- Well-defined linear response is found
- Output is close to 0 V when FSR is not pressed (ie. Rfsr = infinite resistance)
All three of these objectives were acheived when I found the ratio or R2/R1 to be 1k/1.3k. The circuit was updated in the design doc.

# 2/22/2022 - Design Doc Check
After listening to Prof. Schuh's feedback, we'll need to change our high-level requirements such that the units are intuitive and make more sense. Instead of using weight, it would be mroe accurate to use pressure since the weight is being applied over a defined sensing area of the FSR.

# 2/23/2022 - TA Meeting
Went over the Design Doc with Hanyin to gather her feedback, as well as review any missing info to add before the submission deadline tomorrow. No changes were needed.

# 2/24/2022 - FSR Research
Was looking for ideal FSR candidates for use within our project. As of now, most FSRs part of the Arduino Project toolset had too little of a sensing range (ie. they were only meant to detect touch instead of sensing weight/pressure). 

# 2/27/2022 - Finalizing PCB Design
Looked over implementing the amplification circuit on the PCB before the design review. Ritvik was finishing up implementing the power distribution subsystem onto the PCB - however, we had kept running into issues of certain parts going out of stock, which required constant revision of our PCB design.

# 3/1/2022 - Editing PCB Design
Based on the feedback of the TA, our PCB looks to be in good shape. However, there are certain areas of the board in which the trace density was too high, and so some parts may need to be rerouted such that they are not too close to the 5V rail. This will decrease the chances of the board shorting.

# 3/22/2022 - Parts have arrived!
We had just received the parts ordered before Spring break. The first priority was to establish the feasibility of using Bluetooth Low Energy (BLE) for use in transmitting pressure data from the PCB to a phone on an Android app. If this can be established, there will be no need for us to work on the microSD subsystem, which locally stores our data onto the PCB. This will also greatly increase the usefulness of our project, as the user can remotely view their foot pressure as they are walking.

# 3/24/2022 - Editing LTspice sim
As Ritvik had opted for using a different type of battery (LiPo cell instead of Nickel coin batteries), a few changes had to be made for the power subsystem. As such, the new requirements are that the input voltage recieved by the Data Acquisition subsystem is 3.3V instead of 5V. After playing around with the sim, I had found that the new R2/R1 ratio will be 33k/47k ohms. This gives us a maximum Vout of ~2V, while keeping Vout ~0V when the FSR is not pressed. 

# 3/27/2022 - FSR breadboard testing
Now that the FSRs have shipped, I can begin testing them to establish their parameters and test the LTspice circuit on the breadboard. My objectives are the following:
- Observe similar behavior between LTspice sims and real-life results
- Gather measurements of the FSR response when the sensor is fully pressed vs. not pressed at all.
I had designed the breadboard circuit to use a different op-amp than what was listed in the circuit (LM340 instead of LT1014). After designing the circuit, I had set the power supply to 3.3V and had connected an oscilloscope to measure the FSR response, and had gotten the following:
FSR depressed --> 0.127 V
FSR fully pressed --> 2.005 V
This was almost identical to the response seen in the LTspice sims for the amplification circuit. This procedure was repeated for the five other FSRs, each receiving similar values within ~1% error difference between the first FSR values mentioned on the top.

# 3/28/2022 - FSR Linearity Testing
Now that the FSR low and high range have been tested, it was time to establish a range of weights in which a linear relationship can be established between the weight applied to the FSR and the corresponding voltage response. I had used the following procedure:
- An empty container was weighed (4.5 grams) and placed on the FSR, which elicited no response. This served as our first data point - and since there was no change in reponse, it was assumed that we had not yet reached the minimum activation force of the FSR.
- Water was then poured in 15 mL increments into the container. The container was weighed after pouring the water, and then placed onto the FSR. The minimum activation weight was found to be 18.5 grams
- This procedure was repeated until the maximum output voltage (2.005 V) was reached, which was when the container + water weighed 254.5 grams. This was the value in which the FSR entered saturation, and each increase in pressure applied to the FSR would result in no change in output characteristics.
After plotting the data obtained from this experiment into an Excel spreadsheet, a clear linear range was established between weight and output voltage. This was exciting to see, as we can directly correspond the output voltage to the weight applied to the FSRs with little error.

# 3/29/2022 - FSR testing with Shoe
After verifying the FSR characteristics, it was time to move the testing into the shoe insole. The FSR terminals on the breadboard were replaced with jumper cables which connected to the FSR placed underneath the shoe insole. The output terminal was connected to an oscilloscope. Only one FSR was tested to minimize loss in case the FSR was damaged by our testing. The FSR was placed in the heel region, since we expect to find the highest foot pressure in that area. 
After pressing the heel multiple times, and attempting to recreate a walking motion, we were able to get good responses on the oscilloscope, indicating that the FSR exhibits the same functionality when placed within a shoe. In particular, we were able to see exactly when the foot both touched and left the ground in a walking motion, as the oscilloscope registered a gradual increase in pressure when my shoe hit the ground, followed by a corresponding decrease as my foot left the ground. This was particularly important, as we can map the change in pressure in a normal walking stride.

# 4/6/2022 - Shoe insole construction
Now that the FSRs work as intended, it was time to begin work on building the shoe insole. I labeled the FSRs # 1-6, which were placed in the exact same layout as outlined in our design docs. We soldered wires of different colors (each color corresponds to the sensor) to each terminal of the sensor, and heat-shrank the interface between the FSR and the wire connection to incease durability. Afterwards, I attached the FSRs to the shoe insole using black duck tape, and placed the sensing plane upwards facing the shoe insole. This was done row-by-row, as to ensure that no wires were overlapping any of the FSRs - which could cause multiple FSRs to respond when pressure is only applied to one of the sensors. The wires were carefully cable-managed to come out of the right side of the right shoe, so they can be attached to the ribbon cables connecting to the PCB enclosure (still needs to be done). The wires were trimmed to ensure that all wires were of the same length when emerging out of the shoe insole. 
When testing the wearability of the shoe, the wires, which were routed underneath the shoe, could not be felt. This was particularly important, as we want to ensure that the comfort of wearing the IntelliSOLE is not compromised. 

# 4/9/2022 - Testing the shoe insole, sending PCB enclosure CAD for printing
The FSRs attached onto the shoe insole were tested to ensure that connections were secure, and that the behavior of the FSRs remain unchanged. Using the breadboard circuit, each FSR was individually tested by connecting jumper cables from the FSR ports on the breadboard to the connections on the shoe insole, and probing the voltage output using an oscilloscope. All FSRs exhibited the same response except for Sensor 5, which seemed to exhibit floating ground characteristics and not respond when pressure is applied. 
Upon closer inspection, a tear in the printed wiring within the FSR was found. As this wiring was flat and laminated in a plastic covering, soldering was not possible to fix the broken connection. As such, only five pressure sensors were operational. This could be attributed to the differences in tensile strength between the soldered wires (round, thick and durable) and the printed wires within the FSR assembly. 
After testing, I had visited the IIllinois Makerspace Lab to upload our CAD drawings for 3D printing, which was estimated to be complete by the end of next week.

# 4/14/2022 - Durability Testing
The shoe insole was continuously worn and tested for roughly two hours. During walking, I had wanted to observe how comfortable it was to wear the shoe during normal use. While walking, I had noticed that while I did not feel any wires or FSRs housed underneath the insole, the sole felt higher than the other shoe pair with nothing underneath the insoles. When removing the insole upon further inspection, I had found that the wires from Sensor 2 and Sensor 5 had overlapped onto each other. To seperate them and ensure that no wires overlap on each other, I rerouted Sensor 2's wire to follow closer to the edge of the insole. This was easier to do as Sensor 4 was no longer operational, and so it would make no difference if I had run wires over the sensing plane. 
After completing the fix, I went for brief runs and found no issues in comfort. When jumping up and down, I had felt some of the wires sticking out of the shoe insole scratching my lower ankle. This was not an issue, as those exposed ends will be connected to the ribbon cables, which were lower in profile.
When completing these tests, I had restested the FSRs to see if the sensor response had changed, which it did not for all sensors. Same degree of linearity was observed as well.

# 4/20/2022 - Testing of Bluetooth Subsystem
The FSRs were connected to the breadboard circuit via alligator cables to test out the functionality of the Bluetooth subsystem. After connecting the power supply and oscilloscope to the breadboard, the analog outputs seen on the oscilloscope were to be corresponded to the response seen on the Bluetooth subsystem. While the oscilloscope read good outputs based on my stepping and walking motions, the user interface did not register all instances of pressure being applied. This could be attributed to the sampling rate of the Signal processing subsystem, where the sampling resolution was not set high enough. After a few software fixes, responses were able to be seen on the User Interface which corresponded to the oscilloscope reponse. 

# 4/21/2022 - Mock Demo
We demoed the current state of our project with Hanyin and Prof. Song. As our PCB case hadn't arrived yet, we had to conduct our testing on the breadboard circuit as there was no way of holding the PCB next to the shoe. One of the FSRs were tested, and was connected to the breadboard circuit via jumper cables. 

# 4/22 - 4/24/2022 - Arrival of PCB case, Final Product Integration
We've picked up the PCB case on Friday, and work began right away on integrating the shoe insole to the PCB. All ten wires (2 for each sensor) were attached to the exposed wires on the shoe insole, and routed into the PCB enclosure entrance. These cables were then connected to the FSR ports onto the PCB for testing.
When testing the FSR voltage response on the PCB, we had noticed that one of the op-amps rapidly got hot in temperature. When reviewing the PCB design, it was found that the PCB was soldered on backwards, and that the ground line was receiving power. After desoldering re-attaching the op-amp, we'd noticed that we were getting constant high outputs (2.1 V) for some of the FSRs, while one of the FSRs remained at ground, with no change in applying pressure. Only two out of the five FSRs had the desired voltage response. When checking the components on the PCB, nothing seemed to have shorted or heated up. 
After more testing, we had localized the issue to that of the amplifiers. When reviewing the PCB design, it was found that there was a mismatch in footprints of the op-amps in the KiCAD design. Due to time constraints, we had chosen to migrate the Data Acquisition circuitry onto a smaller breadboard, while the PCB was used for signal processing and power distribution.
The amplification circuitry was then compressed onto a much smaller breadboard, and was extensively cable-managed in order to fit into the PCB enclosure. The FSR voltage response was tested using the LiPo battery as the power source, and was found to have identical responses as when using the power supply. This was important because it showed that the Power distribution circuitry was regulating the voltage output of the LiPo cell to 3.3 V. 
Once the Data Acquisition and Power Distribution circuits were shown to work, the Signal Processing and Bluetooth subsystems were tested. After much trial and error, the User Interface was able to show similar voltage responses as the oscilloscope - establishing successful analog-to-digital conversion, signal processing, and Bluetooth connectivity. 





