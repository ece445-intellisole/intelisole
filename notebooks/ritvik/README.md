# 01/21/2022 - Discuss InteliSOLE Implementation

We have multiple strain gauges connected to a MCU which determines pressure based on a change in resistance. The pressure value can be arbitrary (like 0 to 100) depending on if there's any force on it. Once the MCU obtains the pressure values it stores it in onboard memory. When the MCU connects to a phone it send all the pressure data stored onboard to the phone and then the phone app uses the stored data to make a heatmap of pressure.

![](whiteboard_1-22-22.jpg)

# 02/06/2022 - Subsystem Deligation and Design

We needed to pick the approriate components for each subsystem which are in stock. The semiconductor shortage is a big conern at this point in time as semiconductor devices are shipping with long lead times.

I've compiled a list of all the parts and resources in the form of guide we've discovered during the meeting into the list below.

## Subsystem 1: Pressure Data Aq
DIY Resistive Sensor Guide:
This guide walks through how to make your own resistive sensor. We can take inspiration from this sensor to create our own sole.

https://www.instructables.com/O-mat/ 

## Subsystem 2: Power Distribution
Battery Solutions:
General Guide on Batteries:
https://batterysolutions.com/learning-center/battery-types/ 
CR2032:
https://data.energizer.com/pdfs/cr2032.pdf 

6V to 3.3V step-down converter:
https://www.ti.com/product/TPS62203 

## Subsystem 3: Signal Processing
Resistive Sensing Design Guide:
https://www.tarthorst.net/reading-resistive-sensors-with-microcontrollers/ 
https://learn.sparkfun.com/tutorials/analog-to-digital-conversion/relating-adc-value-to-voltage 

MCU
https://www.ti.com/product/MSP430F5659#design-development

## Subsystem 4: Data Storage
SD/microSD Interfacing guide with microcontroller
https://openlabpro.com/guide/interfacing-microcontrollers-with-sd-card/

## Subsystem 5: Data Transmission (OPTIONAL)
Bluetooth Module
https://www.ti.com/product/CC2564 
https://www.ti.com/product/CC2564MODN 


# 02/15/2022 - Scheduling and Planning working times

We discussed material acquisition the sole sensor designed by us for resisitive force sensing. The biggest concern involves acquiring OpAmps for analog signal amplification. Additionally we have a finalized parts list of materials we will acquire but have not ordered yet. The plan is to place orders for the finialized parts by Friday night (02/18/2022). 


# 02/18/2022 - Ordering first batch of components

Finalized list of parts to to be ordered.

Operational Amplifiers - Op Amps Quadruple Op Amp	Texas Instruments	2-4 Days	https://www.mouser.com/ProductDetail/Texas-Instruments/LM324PWRG3?qs=sGAEpiMZZMsvbV9l%252Bnr01EAQhoQi00vOs5IbW1R2Z6A%3D
Interlink Electronics FSR 402 Force-sensing Resistor	Interlink Electronics		https://www.digikey.com/en/products/detail/interlink-electronics/30-81794/2476468
BAT-HLD-003-SMT	Linx Technologies Inc.	2-4 Days	https://www.digikey.com/en/products/detail/linx-technologies-inc/BAT-HLD-003-SMT/14301785
BATT HOLDER COIN 20MM 1 CELL SMD	Keystone Electronics	2-4 Days	https://www.digikey.com/en/products/detail/keystone-electronics/1058/5255484
BATTERY LITHIUM 3V COIN 20MM		2-4 Days	https://www.digikey.com/en/products/detail/zeus-battery-products/CR2032/9828855
Switching Voltage Regulators 3.3V Out Hi-Eff Step-Down Converter
	Texas Instruments		https://www.mouser.com/ProductDetail/595-TPS62203DBVT

# 02/22/2022 - Power Distribution Subsystem Design

The power distribution subsystem is designed with a CR2032 coin cell battery in mind. 
A single CR2032 battery outputs 3V with a recommended current limit around 125mA.
Two CR2032 batteries in series can supply 6V at 125mA. We can use an low-dropout regulator to step down and regulate the voltage from 6V to 5V. The 5V rail can be stepped down to 3.3V using a switching regulator or another LDO. A switching regulator would lead to higher efficiency by cause high frequency noise on the 3.3V power lines.
	
# 02/26/2022 - Redesigning to reduce current consumption

The CR2032 3V coin cell battery supplies a peak current of 125mA. This poses a concern during write cycles to microSD while bluetooth is broadcasting. The bluetooth transmission and external storage systems are by far the most power consuming systems. Bluetooth transmission peaks at ~200mA. Writing to microSD is more variant with less exact information from our specific manufacturer. A general rule of thumb suggests around 150mA used by microSD with a peak ~450mA.
After some research I have a few options.
1) Replace the microcontroller to allow integrated bluetooth communication
2) Replace the battery which increases the current constraints limiting the system
3) Find a way to implement Bluetooth Low Energy to reduce current consumption
4) Use a diffrerent external storage device that reduces current draw during write cycles

# 02/29/2022 - Shortage Problems

While the MSP430F5659 is in stock, it's corresponding low power bluetooth module is out of stock. Similarly compatible LDOs keep going out of stock. If the bluetooth module does not come back into stock within the next few days, we will have to pivot.

Potential future options:
1) Pivot to a different bluetooth module. Any bluetooth module we find will have to communicate with the microcontroller via SPI or I2C. I worry about firmware compaitbility depending on which bluetooth module we switch to.
2) Change the MCU to find an option with an onboard bluetooth module. This would be the ideal option but we will have to redesign the whole systems around the new MCU.
3) Omit the bluetooth subsystem and pivot to ustilizing the external storage module. 

# 03/02/2022 - Replacing the MCU

I've searched for the bluetooth module to come back into stock with no avail. The lead times range from 50 - 60 weeks so I won't hold my breath. Many alternative bluetooth modules supporting SPI or I2C also seem to be out of stock.

Luckily we discovered the ESP32 MCU with modules that support onchip WiFI and Bluetooth capabilities. The ESP32 is also in stock so this should be a viable alternative.

# 03/02/2022 - Redesigning the power distribution system

Since the MCU was replaced I need to redesign the entire power distribution system fropm scratch. The ESP32 can function via 3.3V. The microSD and the sensors are still using 5V but both can be redesigned to use 3.3V. Removing the 5V rail from the design should really reduce power consumption and strech the battery life.

For proper ESP32 functionality, the datasheet recomends the availability of 500mA to the MCU during boot before going into sleep mode. A 600mA current limit element should provide over-current saftey. 
My next concern is that the CR2032 battery has a output current max around 125mA. According to the datasheet of the ESP32, if we use sleep mode for converting ADC data, we can comfortably function under 125mA but I am concerened about power consumption during boot.

To address any brown out concerns during boot, I plan on using four CR2032 batteries in a 2s2p configuration (2 series and 2 parallel). The battery configuration should output 6V and 250mA.
I ran some LTSpice simulations with the CR2032 and the output transient current is above 250mA and 6V. 

# 03/03/2022 - PCB Design

The first round PCB has alot of design considerations with it. I decided to take on the challenge of designing our own PCB antenna trace. In order to provide proper RF shielding I decided to make our board 4 layers. The top layer is the RF transmitting layer. The second layer contains a ground plane to prove RF shielding from trace signals. The third layer contains the data traces and the bottom layer contains the power and most of the analog componenets.

The variant of the ESP32 we designed for has a very small footprint. The trace width from the ESP32 signals are 7 mils which is very concerning but they should be functional.

# 03/04/2022 - Redesigning the power distribution... again
The voltage switching voltage regulator to step down from 5V to 3.3V is out of stock. I found two more voltage regulators that should work according to LTSpice but now I face another issue. Both of these components require inductors that are out of stock. 

The alternative is to use another LDO to drop from 5V to 3.3V. I am now trying to find the appropriate LDO to step down from 5V to 3.3V.

# 03/05/2022 - PCB layout
Bootstrapping pins from the ESP32 have their own breakout pins.
I spent most of the today laying out the PCB. I am growing more concerned about the footprint of our microcontroller.
The MCU is connected to the power ditribution system and the data aquitionstion system still need to be layed out.

# 03/06/2022 - PCB layout and Enclosure Design
I designed a general PCB enclosure to house our board along with some space for the battery. The PCB enclosure needs a way to secure itself. I need to design either a latch into the pastic or design slot for velcro straps.
I continued to layout the PCB and start via stiching the ground planes.
![](PCB_rev1.png)

 
# 03/21/2022 - PCB_rev2 Design
To address the trace width concerns of the first revision of the board, in the second revision we decided to use a larger module to support wider traces. Additionally, the new module also had a built in antenna to simply the RF design on the PCB.

# 03/23/2022 - Power Distribution Resdesign... again...
The new ESP32 module has higer current requirements during boot so the current battery solution needs to be changed. I replaced the battery with a 3.8V LiPo battery rated at 1200mAh. 

This means we need a new voltage regulator solution. A switching regulator to step down from 3.6V - 4.2V to 3.3V would be ideal but an LDO will also do the trick.

# 03/26/2022 - 03/28/2022 - PCB_rev2 layout
Most of this time was spent redesiging the layout of the board with a new ESP32 module and a power distribution solution.

The board layout was simplified to only two layers.

![](PCB_rev2_front.png)
![](PCB_rev2_back.png)


# 03/28/2022 - Signal Processing
A general conversion implementation was tested using an ESP32 devkit. It also included a filter design with an averaging filter.

# 04/05/2022 - Redesigned the Enclosure
The PCB enclosure was redesigned with a lip and latches on all four sides to securely and tighly enclose the electronics. The lip acts like a gasket and provides a waterproofing seal nd stops debris to get into the enclosure.

The PCB can be printed using PLA on a 3D printer.

![](PCB_Enclosure.png)

# 04/14/2022 - Soldering
I soldered the switching regulator on PCB.
When I tried to verify the output of the regulator solution, the output voltage read to be 1.8V. After debugging for a bit, I realized the issue was due to the fact that the IC we had was the 1.8V variant but we designed for the 3.3V variant.
 
![](PD_V_Switching.jpg)

# 04/15/2022 - Soldering
I soldered the LDO and was able to verify a regulated 3.3V on the power line.
![](PD_V_LDO.jpg)

Soldered the ESP32 on board but I could not verify functionality of the ESP32.
![](Soldered_ESP32.jpg)

# 04/16/2022 - Soldering
I verified that the ESP32 can go into boot mode. Most of today was spend soldering the data aq. components.

# 04/18/2022 - UART Flashing Issue
We expereienced issues flashing the ESP32. I followed standard ESP32 procedure with the proper strapping pins jumped. I noticed the enable line was getting pulled low and back high implying the ESP32 was constantly rebooting.

# 04/20/2022 - Replaced ESP32 Module
The unusual behavior by the ESP32 was fixed by replacing the old module with a new one. I attribute the cause of the malfunctioning ESP32 to some solder paste getting under the module and shorting the enable pin with the power rail. 