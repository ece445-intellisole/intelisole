EN Issue:
- Enable is pulled low --> Shorted w/ ground?
- Removed pull up (R1) and still shorted with ground
- C3 shorting EN?

Op Amp Issue: (FIXED)
- IC2 hot/slight smoke
- IC2 was soldered incorrectly as it was rotated 180 degrees 
- The IC2 label is orientied in the opposite direction leading to confusion

Op Amp Issue:
- The footprint layout is inconsistent with the true pin layout
- Scratch off connection and jump the correct connections?

Voltage Regulator:
- Voltage regulator output leg was broken off after desoldering the componenent
- Temporary fix by replacing the regulator with another regulator from a dev kit but different operating region
