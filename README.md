# InteliSOLE

Team members:

- [Ritvik Avancha (rra2)](https://github.com/average-avancha)
- [Alkesh Sumant (asumant2)](https://github.com/<insert-username>)
- [Xinyang Xu (xinyang8)](https://github.com/<insert-username>)

# Problem

Walking and running are activities many of us take for granted. Yet, any disruption to these activities pose enormous challenges to our daily lives. As such, studying human motion is important in monitoring health and preventing injury. Mapping foot plantar pressure in real-time can provide insights into the wearer's foot posture, and identify potential health issues that stem from having abnormal gait. While there are solutions for foot pressure mapping, such as pressure mats and treadmills, these are often high-cost products that are not easily accessible to the average person. Current pressure-mapping shoe insoles on the market also suffer from the issue of high-cost, and face many complaints of being uncomfortable and bulky to wear.

# Solution

Our proposal is to provide a low-cost, comfortable solution to mapping foot plantar pressure in real-time and provide the average user with information regarding their foot posture without having to see a doctor or spend lots of money on. We plan to develop a shoe insole sensor paired with an app that allows the user to see their foot pressure in real time, and can be inserted into any shoe.

# Solution Components
## Component List:
MCU - MAX32666GWP+ (Analog Devices),
Resistors - 1K Ohm Resistor (SMD),
Multiplexers - HP4067 (Texas Instruments),
Semiconductive Material - Velostat,
Ribbon Cables - Flat 24 Gauge Ribbon Cables,
Conductive Material - Copper Tape,
Battery - Rechargeable NiMH

## Subsystem 1 - Pressure Data Acquisition
The Pressure Data Acquisition subsystem is made up of a resistive matrix mat in the shape of a sole. The matrix consists of a layer of conductive copper rows and a layer of conductive copper columns with semiconductive material in between, such as Velostat. When pressure is applied to the resistive matrix, the corresponding row and column short a resistor in parallel. This short allows for detection of the applied pressure as a cartesian coordinate along the resistive matrix. The number of rows and columns scale directly with the resolution in the sensing. 

Expected Subsystem Components:
MCU - MAX32666GWP+ (Analog Devices),
Resistors - 1K Ohm Resistor (SMD),
Multiplexers - HP4067 (Texas Instruments),
Semiconductive Material - Velostat,
Ribbon Cables - Flat 24 Gauge Ribbon Cables,
Copper Tape

## Subsystem 2 - Power Distribution
The power distribution subsystem is designed to distribute power from a reachable NiMH battery. Each NiMH cell outputs 1.2V which means 4 cells in series would produce 4.8V which would have to be stepped down to 3.3V to power the MCU.

Expected Subsystem Components:
Battery - Rechargeable NiMH


## Subsystem 3 - Signal Processing Module
The signal processing module is in charge of converting the analog data into a digital signal and applying signal filtering to reduce noise before the data is stored by the Data Storage subsystem. The analog data will be converted into a digital value by the on-chip ADC on the MCU.

Expected Subsystem Components:
ADC - Exists on-chip with the MCU

## Subsystem 4 - Data Storage and Transfer (via Bluetooth) Module
This subsystem is in charge of storing and retrieving the data after data goes through signal processing. Once the data is ready to be offloaded onto the user interface, the subsystem shares the data via bluetooth to the user’s smart device.

Expected Subsystem Components:
Flash Storage - On-chip with the MCU,
Bluetooth Module - On-chip with the MCU

The completion of this subsystem is a stretch goal. In the interest of time, we can use a script in realtime to generate the heatmap.

## Subsystem 5 - User Interface/Data Processing (OPTIONAL)
The user interface is a front-end to connect the user and the sensor with ease. The objective of this subsystem is to generate a heatmap for the given pressure data acquired through bluetooth connection and data processing to make it readable.

The completion of this subsystem is a stretch goal. In the interest of time, we can use a script in realtime to generate the heatmap useful for users to interpret data collected. 

Expected Subsystem Components:
Smart Phone


# Criterion For Success

The baseline criterion for success is to be able to generate a readable heatmap mapping plantar foot pressure of the entire foot, and have a clear enough resolution to identify potential health issues, such as flat feet or foot ulcers. To be successful, the embedded processing (taking in high-frequency data from sensors and averaging out resistance values) and software post-processing (reading averaged sensor data to generate a heatmap) must be operational. High pressure points must be clearly marked in red, low pressure areas must be marked in blue, and there must be a gradient mapping the changes in pressure between the two regions. 
