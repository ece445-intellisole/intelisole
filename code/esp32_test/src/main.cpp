#include <BLEDevice.h>
#include <BLEUtils.h>
#include <BLEServer.h>
#include <Arduino.h>
#include "FS.h"
#include "SD.h"
#include <SPI.h>

// Bluetooth Definitions
#define SERVICE_UUID "4fafc201-1fb5-459e-8fcc-c5c9c331914b"
#define CHARACTERISTIC1_UUID "beb5483e-36e1-4688-b7f5-ea07361b26a8"
#define CHARACTERISTIC2_UUID "b2c19c80-b85e-11ec-b909-0242ac120002"
#define CHARACTERISTIC3_UUID "07789ee6-bc6e-11ec-8422-0242ac120002"
#define CHARACTERISTIC4_UUID "1f7b64f6-bc6e-11ec-8422-0242ac120002"
#define CHARACTERISTIC5_UUID "29909344-bc6e-11ec-8422-0242ac120002"
#define CHARACTERISTIC6_UUID "2f7a411a-bc6e-11ec-8422-0242ac120002"
// Pin Definitions
#define DATA1_GPIO 36
#define DATA2_GPIO 39
#define DATA3_GPIO 34
#define DATA4_GPIO 35
#define DATA5_GPIO 32
#define DATA6_GPIO 33
#define SD_CS 5
// Parameter Definitions
#define FILTER_SIZE 16
// Save reading number on RTC memory
RTC_DATA_ATTR int readingID = 0;

String dataMessage;
int8_t dataIdx;
// uint8_t data1 [FILTER_SIZE];
// uint8_t data2 [FILTER_SIZE];
// uint8_t data3 [FILTER_SIZE];
// uint8_t data4 [FILTER_SIZE];
// uint8_t data5 [FILTER_SIZE];
// uint8_t data6 [FILTER_SIZE];
uint8_t data1;
uint8_t data2;
uint8_t data3;
uint8_t data4;
uint8_t data5;
uint8_t data6;
uint16_t data1_raw;
uint16_t data2_raw;
uint16_t data3_raw;
uint16_t data4_raw;
uint16_t data5_raw;
uint16_t data6_raw;
BLECharacteristic pCharacteristic1(CHARACTERISTIC1_UUID, BLECharacteristic::PROPERTY_READ);
BLECharacteristic pCharacteristic2(CHARACTERISTIC2_UUID, BLECharacteristic::PROPERTY_READ);
BLECharacteristic pCharacteristic3(CHARACTERISTIC3_UUID, BLECharacteristic::PROPERTY_READ);
BLECharacteristic pCharacteristic4(CHARACTERISTIC4_UUID, BLECharacteristic::PROPERTY_READ);
BLECharacteristic pCharacteristic5(CHARACTERISTIC5_UUID, BLECharacteristic::PROPERTY_READ);
BLECharacteristic pCharacteristic6(CHARACTERISTIC6_UUID, BLECharacteristic::PROPERTY_READ);

void setup()
{
  Serial.begin(115200);

  dataIdx = 0;
  data1 = 0;
  data2 = 0;
  data3 = 0;
  data4 = 0;
  data5 = 0;
  data6 = 0;
  data1_raw = 0;
  data2_raw = 0;
  data3_raw = 0;
  data4_raw = 0;
  data5_raw = 0;
  data6_raw = 0;
  // data1 [FILTER_SIZE] = {0};
  // data2 [FILTER_SIZE] = {0};
  // data3 [FILTER_SIZE] = {0};
  // data4 [FILTER_SIZE] = {0};
  // data5 [FILTER_SIZE] = {0};
  // data6 [FILTER_SIZE] = {0};

  BLEDevice::init("ECE445Team18");
  BLEServer *pServer = BLEDevice::createServer();
  BLEService *pService = pServer->createService(SERVICE_UUID);

  // BLECharacteristic *pCharacteristic1 = pService->createCharacteristic(
  //     CHARACTERISTIC1_UUID,
  //     BLECharacteristic::PROPERTY_READ |
  //         BLECharacteristic::PROPERTY_WRITE);
  // pCharacteristic1->setValue(&data1, sizeof(data1));
  pService->addCharacteristic(&pCharacteristic1);
  pService->addCharacteristic(&pCharacteristic2);
  pService->addCharacteristic(&pCharacteristic3);
  pService->addCharacteristic(&pCharacteristic4);
  pService->addCharacteristic(&pCharacteristic5);
  pService->addCharacteristic(&pCharacteristic6);

  // BLECharacteristic *pCharacteristic2 = pService->createCharacteristic(
  //     CHARACTERISTIC2_UUID,
  //     BLECharacteristic::PROPERTY_READ |
  //         BLECharacteristic::PROPERTY_WRITE);
  // pCharacteristic2->setValue("7890");
  pService->start();

  BLEAdvertising *pAdvertising = pServer->getAdvertising();
  pAdvertising->start();
}

void loop()
{
  // put your main code here, to run repeatedly:
  data1_raw += analogRead(DATA1_GPIO); // read value into a 16 bit holder
  data2_raw += analogRead(DATA2_GPIO);
  data3_raw += analogRead(DATA3_GPIO);
  data4_raw += analogRead(DATA4_GPIO);
  data5_raw += analogRead(DATA5_GPIO);
  data6_raw += analogRead(DATA6_GPIO);

  dataIdx++;
  if (dataIdx >= FILTER_SIZE)
  {
    data1_raw /= FILTER_SIZE; // read value into a 16 bit holder
    data2_raw /= FILTER_SIZE;
    data3_raw /= FILTER_SIZE;
    data4_raw /= FILTER_SIZE;
    data5_raw /= FILTER_SIZE;
    data6_raw /= FILTER_SIZE;
    data1_raw = data1_raw / 16; // recast 16bit to 8bit by right shifting 8 bits
    data2_raw = data2_raw / 16;
    data3_raw = data3_raw / 16;
    data4_raw = data4_raw / 16;
    data5_raw = data5_raw / 16;
    data6_raw = data6_raw / 16;
    data1 = data1_raw;
    data2 = data2_raw;
    data3 = data3_raw;
    data4 = data4_raw;
    data5 = data5_raw;
    data6 = data6_raw;
    // data1 = 2;
    (&pCharacteristic1)->setValue(&data1, sizeof(data1));
    (&pCharacteristic2)->setValue(&data2, sizeof(data2));
    (&pCharacteristic3)->setValue(&data3, sizeof(data3));
    (&pCharacteristic4)->setValue(&data4, sizeof(data4));
    (&pCharacteristic5)->setValue(&data5, sizeof(data5));
    (&pCharacteristic6)->setValue(&data6, sizeof(data6));

    dataIdx = 0;
    data1_raw = 0; // read value into a 16 bit holder
    data2_raw = 0;
    data3_raw = 0;
    data4_raw = 0;
    data5_raw = 0;
    data6_raw = 0;
  }
}

// Append data to the SD card (DON'T MODIFY THIS FUNCTION)
// void appendFile(fs::FS &fs, const char *path, const char *message)
// {
//   Serial.printf("Appending to file: %s\n", path);

//   File file = fs.open(path, FILE_APPEND);
//   if (!file)
//   {
//     Serial.println("Failed to open file for appending");
//     return;
//   }
//   if (file.print(message))
//   {
//     Serial.println("Message appended");
//   }
//   else
//   {
//     Serial.println("Append failed");
//   }
//   file.close();
// }

// Write the data on the SD card
// void logSDCard()
// {
//   dataMessage = String(data1) + "\r\n";
//   Serial.print("Save data: ");
//   Serial.println(dataMessage);
//   appendFile(SD, "/data.txt", dataMessage.c_str());
// }

// Write to the SD card (DON'T MODIFY THIS FUNCTION)
// void writeFile(fs::FS &fs, const char *path, const char *message)
// {
//   Serial.printf("Writing file: %s\n", path);

//   File file = fs.open(path, FILE_WRITE);
//   if (!file)
//   {
//     Serial.println("Failed to open file for writing");
//     return;
//   }
//   if (file.print(message))
//   {
//     Serial.println("File written");
//   }
//   else
//   {
//     Serial.println("Write failed");
//   }
//   file.close();
// }
