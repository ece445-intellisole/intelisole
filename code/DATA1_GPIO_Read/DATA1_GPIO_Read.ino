// Pins
#define DATA1_GPIO 36 
#define DATA2_GPIO 39
#define DATA3_GPIO 34
#define DATA4_GPIO 35
#define DATA5_GPIO 32
#define DATA6_GPIO 33

// Parameters
#define FILTER_SIZE 16

uint16_t data1;
uint16_t data2;
uint16_t data3;
uint16_t data4;
uint16_t data5;
uint16_t data6;

int8_t dataIdx;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);
  data1 = 0;
  data2 = 0;
  data3 = 0;
  data4 = 0;
  data5 = 0;
  data6 = 0;

  dataIdx = 0;
  
  delay(1000);
}

void loop() {
  // put your main code here, to run repeatedly:
  data1 += analogRead(DATA1_GPIO);
  data2 += analogRead(DATA2_GPIO);
  data3 += analogRead(DATA3_GPIO);
  data4 += analogRead(DATA4_GPIO);
  data5 += analogRead(DATA5_GPIO);
  data6 += analogRead(DATA6_GPIO);

  dataIdx++;
  if(dataIdx >= FILTER_SIZE){
    dataIdx = 0;

    data1 /= FILTER_SIZE;
    data2 /= FILTER_SIZE;
    data3 /= FILTER_SIZE;
    data4 /= FILTER_SIZE;
    data5 /= FILTER_SIZE;
    data6 /= FILTER_SIZE;
    
    Serial.println(data1);
//    Serial.println(data2);
//    Serial.println(data3);
//    Serial.println(data4);
//    Serial.println(data5);
//    Serial.println(data6);
    
    data1 = 0;
    data2 = 0;
    data3 = 0;
    data4 = 0;
    data5 = 0;
    data6 = 0;
  }
}
